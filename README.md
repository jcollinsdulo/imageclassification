# Image Classification Using Neural Networks

This is the repo to the 2-part series I made on Medium

# Links to the post are here
Part 1 : https://medium.com/@Job_Collins/part-one-image-classification-with-tensorflow-python-f92f94121ec1

Part 2 : https://medium.com/@Job_Collins/part-two-image-classification-with-tensorflow-python-facfe535b2d2

# While viewing the ipynb file change from default file viewer to ipython notebook

# or

follow this gist https://gist.github.com/datasto/cb1d018bc24c5f0f8ce4f14c40e1424c
